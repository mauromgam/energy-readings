<?php

use App\EnergyType;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class EnergyTypeApiTest extends TestCase
{
    use WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     * @throws Exception
     */
    public function testCreateEnergyType()
    {
        $data = $this->generateFakeEnergyType();

        $response = $this->postJsonAuth(
            '/api/v1/energy-types',
            $data
        );

        $response->assertSuccessful();
        $response->assertJson([
            'data' => (new EnergyType($data))->toArray(),
        ]);
    }

    /**
     * @test
     * @throws Exception
     */
    public function testCreateEnergyTypeFail()
    {
        $response = $this->postJsonAuth(
            '/api/v1/energy-types',
            []
        );

        $response->assertStatus(422);
    }

    /**
     * @test
     */
    public function testReadEnergyTypes()
    {
        $response = $this->getJsonAuth(
            '/api/v1/energy-types'
        );

        $response->assertSuccessful();
    }

    /**
     * @test
     */
    public function testReadEnergyType()
    {
        $energyType = $this->createModelsWithFactory(EnergyType::class)->first();
        $response = $this->getJsonAuth(
            '/api/v1/energy-types/' . $energyType->id
        );

        $response->assertSuccessful();
        $response->assertJson([
            'data' => $energyType->toDetailsArray(),
        ]);
    }

    /**
     * @test
     */
    public function testReadEnergyTypeFail()
    {
        $response = $this->getJsonAuth(
            '/api/v1/energy-types/' . str_random()
        );

        $response->assertNotFound();
    }

    /**
     * @test
     */
    public function testUpdateEnergyType()
    {
        $energyType = $this->createModelsWithFactory(EnergyType::class)->first();
        $data = $energyType->toArray();
        $data['name'] = str_random();
        $response = $this->putJsonAuth(
            '/api/v1/energy-types/' . $energyType->id,
            $data
        );

        $response->assertSuccessful();
        $response->assertJson([
            'data' => [
                'name' => $data['name']
            ],
        ]);
    }

    /**
     * @test
     */
    public function testDeleteEnergyType()
    {
        $energyType = $this->createModelsWithFactory(EnergyType::class)->first();

        $response = $this->deleteJsonAuth(
            '/api/v1/energy-types/' . $energyType->id
        );

        $response->assertSuccessful();
    }
}
