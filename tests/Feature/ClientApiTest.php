<?php

use App\Client;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class ClientApiTest extends TestCase
{
    use WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     * @throws Exception
     */
    public function testCreateClient()
    {
        $data = $this->generateFakeClient();

        $response = $this->postJsonAuth(
            '/api/v1/clients',
            $data
        );

        $response->assertSuccessful();
        $response->assertJson([
            'data' => (new Client($data))->toArray(),
        ]);
    }

    /**
     * @test
     * @throws Exception
     */
    public function testCreateClientFail()
    {
        $data['first_name'] = str_random();

        $response = $this->postJsonAuth(
            '/api/v1/clients',
            $data
        );

        $response->assertStatus(422);
    }

    /**
     * @test
     */
    public function testReadClients()
    {
        $response = $this->getJsonAuth(
            '/api/v1/clients'
        );

        $response->assertSuccessful();
    }

    /**
     * @test
     */
    public function testReadClient()
    {
        $client = $this->createModelsWithFactory(Client::class)->first();
        $response = $this->getJsonAuth(
            '/api/v1/clients/' . $client->id
        );

        $response->assertSuccessful();
        $response->assertJson([
            'data' => $client->toDetailsArray(),
        ]);
    }

    /**
     * @test
     */
    public function testReadClientFail()
    {
        $response = $this->getJsonAuth(
            '/api/v1/clients/' . str_random()
        );

        $response->assertNotFound();
    }

    /**
     * @test
     */
    public function testUpdateClient()
    {
        $client = $this->createModelsWithFactory(Client::class)->first();
        $data = $client->toDetailsArray();
        $data['first_name'] = str_random();
        $response = $this->putJsonAuth(
            '/api/v1/clients/' . $client->id,
            $data
        );

        $response->assertSuccessful();
        $response->assertJson([
            'data' => [
                'first_name' => $data['first_name']
            ],
        ]);
    }

    /**
     * @test
     */
    public function testDeleteClient()
    {
        $client = $this->createModelsWithFactory(Client::class)->first();

        $response = $this->deleteJsonAuth(
            '/api/v1/clients/' . $client->id
        );

        $response->assertSuccessful();
    }
}
