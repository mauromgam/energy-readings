<?php

use App\Client;
use App\Reading;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class ReadingApiTest extends TestCase
{
    use WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     * @throws Exception
     */
    public function testCreateReading()
    {
        $data = $this->generateFakeReading();

        $response = $this->postJsonAuth(
            '/api/v1/readings',
            $data
        );

        $response->assertSuccessful();
        $response->assertJson([
            'data' => (new Reading($data))->toArray(),
        ]);
    }

    /**
     * @test
     * @throws Exception
     */
    public function testCreateReadingFail()
    {
        $data['reading'] = str_random();

        $response = $this->postJsonAuth(
            '/api/v1/clients',
            $data
        );

        $response->assertStatus(422);
    }

    /**
     * @test
     */
    public function testReadReadings()
    {
        $response = $this->getJsonAuth(
            '/api/v1/readings'
        );

        $response->assertSuccessful();
    }

    /**
     * @test
     */
    public function testReadReadingsSearchEnergyType()
    {
        /** @var \App\Collections\ReadingCollection $readings */
        $readings = Reading::join('energy_types', 'energy_types.id', '=', 'readings.energy_type_id')
            ->where('energy_types.name', '=', 'Gas')
            ->get();

        $response = $this->getJsonAuth(
            '/api/v1/readings?search=energy_type:gas'
        );

        $response->assertSuccessful();
        $data = json_decode($response->getContent());
        $this->assertEquals($readings->count(), $data->data->total);
    }

    /**
     * @test
     */
    public function testReadReading()
    {
        $reading = $this->createModelsWithFactory(Reading::class)->first();
        $response = $this->getJsonAuth(
            '/api/v1/readings/' . $reading->id
        );

        $response->assertSuccessful();
        $response->assertJson([
            'data' => $reading->toDetailsArray(),
        ]);
    }

    /**
     * @test
     */
    public function testReadReadingFail()
    {
        $response = $this->getJsonAuth(
            '/api/v1/readings/' . str_random()
        );

        $response->assertNotFound();
    }

    /**
     * @test
     */
    public function testReadClientReadings()
    {
        $clientId = Reading::inRandomOrder()->first()->client_id;
        $response = $this->getJsonAuth(
            '/api/v1/clients/' . $clientId . '/readings'
        );

        $response->assertSuccessful();
    }

    /**
     * @test
     */
    public function testReadReadingsSearch()
    {
        /** @var Client $client */
        $client = Reading::inRandomOrder()->first()->client;
        $readings = $client->readings;

        $response = $this->getJsonAuth(
            '/api/v1/readings?search=client:' . $client->email
        );

        $data = json_decode($response->getContent());
        $response->assertSuccessful();
        $this->assertEquals($readings->count(), $data->data->total);
    }

    /**
     * @test
     */
    public function testReadReadingsSearchFail()
    {
        $response = $this->getJsonAuth(
            '/api/v1/readings?search=date:2019-01-01--2018-01-01'
        );

        $data = json_decode($response->getContent());
        $this->assertFalse($data->success);
        $this->assertEquals('Wrong date range entered', $data->message);
    }

    /**
     * @test
     */
    public function testUpdateReading()
    {
        $reading = $this->createModelsWithFactory(Reading::class)->first();
        $data = $reading->toArray();
        $data['client_id'] = Client::inRandomOrder()->first()->id;
        $response = $this->putJsonAuth(
            '/api/v1/readings/' . $reading->id,
            $data
        );

        $response->assertSuccessful();
        $response->assertJson([
            'data' => [
                'client_id' => $data['client_id']
            ],
        ]);
    }

    /**
     * @test
     */
    public function testDeleteReading()
    {
        $reading = $this->createModelsWithFactory(Reading::class)->first();

        $response = $this->deleteJsonAuth(
            '/api/v1/readings/' . $reading->id
        );

        $response->assertSuccessful();
    }
}
