<?php

namespace Tests;

use App\Client;
use App\EnergyType;
use App\User;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Laravel\Passport\ClientRepository;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    /** @var \ReflectionMethod $method */
    protected $method;

    /** @var User $user */
    protected $user;

    /**
     * @var array
     */
    protected $headers = [];

    /**
     * @var array
     */
    protected $scopes = [];


    /**
     * TestCase constructor.
     */
    public function setUp()
    {
        parent::setUp();

        $clientRepository = new ClientRepository();
        $client = $clientRepository->createPersonalAccessClient(null, 'Test Personal Access Client', '/');
        DB::table('oauth_personal_access_clients')->insert([
            'client_id' => $client->getKey(),
            'created_at' => new \DateTime,
            'updated_at' => new \DateTime,
        ]);

        $this->user = $this->getUser();

        $token = $this->user->createToken('TestToken', $this->scopes);

        $this->headers['Accept'] = 'application/json';
        $this->headers['Authorization'] = 'Bearer ' . $token->accessToken;
        $this->headers['X-Requested-With'] = 'XMLHttpRequest';
    }

    /**
     * @return User
     */
    public static function getUser(): User
    {
        return User::inRandomOrder()->first();
    }

    /**
     * @param \ReflectionClass $class
     * @param string $method
     */
    protected function initClassMethod(\ReflectionClass $class, string $method): void
    {
        $this->method = $class->getMethod($method);
        $this->method->setAccessible(true);
    }

    /**
     * @param string $uri
     * @param array $headers
     * @return \Illuminate\Foundation\Testing\TestResponse
     */
    public function getJsonAuth($uri, array $headers = [])
    {
        return parent::getJson($uri, array_merge($this->headers, $headers));
    }

    /**
     * @param string $uri
     * @param array $data
     * @param array $headers
     * @return \Illuminate\Foundation\Testing\TestResponse
     */
    public function postJsonAuth($uri, array $data = [], array $headers = [])
    {
        return parent::postJson($uri, $data, array_merge($this->headers, $headers));
    }

    /**
     * @param string $uri
     * @param array $data
     * @param array $headers
     * @return \Illuminate\Foundation\Testing\TestResponse
     */
    public function putJsonAuth($uri, array $data = [], array $headers = [])
    {
        return parent::putJson($uri, $data, array_merge($this->headers, $headers));
    }

    /**
     * @param string $uri
     * @param array $data
     * @param array $headers
     * @return \Illuminate\Foundation\Testing\TestResponse
     */
    public function deleteJsonAuth($uri, array $data = [], array $headers = [])
    {
        return parent::deleteJson($uri, $data, array_merge($this->headers, $headers));
    }

    /**
     * @param string $model
     * @param int $number
     * @param array $data
     * @return Collection
     */
    public function createModelsWithFactory(string $model, int $number = 1, array $data = [])
    {
        /** @var Collection $models */
        try {
            $models = factory($model, $number)->create($data);
        } catch (\Exception $e) {
            return $this->createModelsWithFactory($model, $number, $data);
        }

        return $models;
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function generateFakeClient()
    {
        return [
            'account_number'       => random_int(10000, 99999),
            'first_name'           => 'Client',
            'last_name'            => 'LastName ' . random_int(2, 9999),
            'phone_number'         => random_int(10000, 99999),
            'email'                => str_random() . '@random.com',
            'default_reading_date' => Carbon::now()->subYear(1)->toDateTimeString(),
            'is_enabled'           => 1,
        ];
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function generateFakeReading()
    {
        $double = mt_rand() / mt_getrandmax();
        $reading = floatval(number_format(random_int(100, 99999) + $double, 4));
        $client = Client::inRandomOrder()->first();
        $energyType = EnergyType::inRandomOrder()->first();

        return [
            'client_id'      => $client->id,
            'energy_type_id' => $energyType->id,
            'reading'        => $reading,
            'date'           => Carbon::now()->toDateTimeString(),
        ];
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function generateFakeEnergyType()
    {
        return [
            'name'       => str_random(),
            'is_enabled' => 1,
        ];
    }
}
