<?php

namespace Tests\Feature;

use App\Client;
use App\Collections\ReadingCollection;
use App\Reading;
use App\Repositories\ReadingRepository;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Tests\TestCase;

class ReadingRepositoryTest extends TestCase
{
    /** @var ReadingRepository $class */
    protected $class;

    public function setUp()
    {
        parent::setUp();

        $this->class = $this->getMockBuilder(ReadingRepository::class)
            ->disableOriginalConstructor()
            ->setMethods()
            ->getMock();
    }

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_model()
    {
        $this->assertEquals(Reading::class, $this->class->model());
    }

    /**
     * @throws \ReflectionException
     */
    public function test_clientFilter()
    {
        $this->initClassMethod(new \ReflectionClass(ReadingRepository::class), 'clientFilter');

        $obj = new ReadingRepository(app());
        $invoked = $this->method->invokeArgs($obj, []);

        $this->assertIsArray($invoked);
        $this->assertNotFalse(in_array('clients.first_name', $invoked));
        $this->assertNotFalse(in_array('clients.last_name', $invoked));
        $this->assertNotFalse(in_array('clients.email', $invoked));
        $this->assertNotFalse(in_array('CONCAT(clients.first_name, \' \', clients.last_name)', $invoked));
    }

    /**
     * Test return of paginated results calling the
     *
     * @return void
     * @throws \ReflectionException
     */
    public function test_getAllPaginated()
    {
        $this->initClassMethod(new \ReflectionClass(ReadingRepository::class), 'getAllPaginated');
        $model = new Reading();
        $request = new Request();
        $columns = ['*'];

        $obj = new ReadingRepository(app());
        $invoked = $this->method->invokeArgs($obj, [$model, $request, $columns]);

        $this->assertInstanceOf(LengthAwarePaginator::class, $invoked);
    }

    /**
     * Test return of paginated results calling the
     *
     * @return void
     * @throws \ReflectionException
     */
    public function test_getClientReadings()
    {
        $this->initClassMethod(new \ReflectionClass(ReadingRepository::class), 'getClientReadings');
        $clientId = Reading::inRandomOrder()->first()->client_id;
        $model = new Reading();
        $request = new Request();

        $obj = new ReadingRepository(app());
        $invoked = $this->method->invokeArgs($obj, [$clientId, $model, $request]);

        $this->assertInstanceOf(ReadingCollection::class, $invoked);
    }
}
