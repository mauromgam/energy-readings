<?php

namespace Tests\Feature;

use App\Client;
use App\Repositories\ClientRepository;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Tests\TestCase;

class ClientRepositoryTest extends TestCase
{
    /** @var ClientRepository $class */
    protected $class;

    public function setUp()
    {
        parent::setUp();

        $this->class = $this->getMockBuilder(ClientRepository::class)
            ->disableOriginalConstructor()
            ->setMethods()
            ->getMock();
    }

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_model()
    {
        $this->assertEquals(Client::class, $this->class->model());
    }

    /**
     * A basic feature test example.
     *
     * @return void
     * @throws \ReflectionException
     */
    public function test_getAllPaginated()
    {
        $this->initClassMethod(new \ReflectionClass(ClientRepository::class), 'getAllPaginated');
        $model = new Client();
        $request = new Request();
        $columns = ['*'];

        $obj = new ClientRepository(app());
        $invoked = $this->method->invokeArgs($obj, [$model, $request, $columns]);

        $this->assertInstanceOf(LengthAwarePaginator::class, $invoked);
    }
}
