<?php

namespace Tests\Feature;

use App\EnergyType;
use App\Repositories\EnergyTypeRepository;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Tests\TestCase;

class EnergyTypeRepositoryTest extends TestCase
{
    /** @var EnergyTypeRepository $class */
    protected $class;

    public function setUp()
    {
        parent::setUp();

        $this->class = $this->getMockBuilder(EnergyTypeRepository::class)
            ->disableOriginalConstructor()
            ->setMethods()
            ->getMock();
    }

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_model()
    {
        $this->assertEquals(EnergyType::class, $this->class->model());
    }

    /**
     * Test return of paginated results calling the
     *
     * @return void
     * @throws \ReflectionException
     */
    public function test_getAllPaginated()
    {
        $this->initClassMethod(new \ReflectionClass(EnergyTypeRepository::class), 'getAllPaginated');
        $model = new EnergyType();
        $request = new Request();
        $columns = ['*'];

        $obj = new EnergyTypeRepository(app());
        $invoked = $this->method->invokeArgs($obj, [$model, $request, $columns]);

        $this->assertInstanceOf(LengthAwarePaginator::class, $invoked);
    }
}
