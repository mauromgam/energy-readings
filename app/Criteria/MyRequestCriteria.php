<?php

namespace App\Criteria;

use App\Utils\StringUtil;
use App\Exceptions\NoSearchDependencyException;
use App\Exceptions\WrongDateRangeException;
use App\Traits\ParseSearchStringTrait;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Criteria\RequestCriteria;

/**
 * Class MyRequestCriteria.
 *
 * @package namespace App\Criteria;
 */
class MyRequestCriteria extends RequestCriteria
{
    use ParseSearchStringTrait;

    /** @var RepositoryInterface */
    protected $repository;

    /**
     * @param RepositoryInterface $repository
     */
    public function setRepository(RepositoryInterface $repository): void
    {
        $this->repository = $repository;
    }

    /**
     * Apply criteria in query repository
     *
     * @param         Builder|Model $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     * @throws \Exception
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $this->setRepository($repository);
        $fieldsSearchable = array_merge(
            $this->repository->getFieldsSearchable(),
            $this->repository->getPersonalisedFieldsSearchable()
        );
        $search = $this->request->get(config('repository.criteria.params.search', 'search'), null);
        $searchFields = $this->request->get(config('repository.criteria.params.searchFields', 'searchFields'), null);
        $filter = $this->request->get(config('repository.criteria.params.filter', 'filter'), null);
        $orderBy = $this->request->get(config('repository.criteria.params.orderBy', 'orderBy'), null);
        $sortedBy = $this->request->get(config('repository.criteria.params.sortedBy', 'sortedBy'), 'asc');
        $with = $this->request->get(config('repository.criteria.params.with', 'with'), null);
        $searchJoin = $this->request->get(config('repository.criteria.params.searchJoin', 'searchJoin'), null);
        $sortedBy = !empty($sortedBy) ? $sortedBy : 'asc';

        if ($search && is_array($fieldsSearchable) && count($fieldsSearchable)) {
            if (!is_array($searchFields) && !is_null($searchFields)) {
                $searchFields = StringUtil::explode('\;', $searchFields);
            }
            $fields = $this->parserFieldsSearch($fieldsSearchable, $searchFields);
            $isFirstField = true;
            $search = StringUtil::removeTimeFromDate($search);
            $search = $this->parserSearchValue($search);
            $this->validateDependencies($search);

            $modelForceAndWhere = null;
            if (!empty($searchJoin)) {
                $modelForceAndWhere = strtolower($searchJoin) === 'and';
            }

            $model = $model->where(function ($query) use ($fields, $search, $isFirstField, $modelForceAndWhere) {
                /** @var Builder $query */

                $modelTableName = $query->getModel()->getTable();

                if ($modelForceAndWhere === null) {
                    $this->setCustomisedSearch(
                        $query,
                        $modelTableName,
                        $fields,
                        $search
                    );
                } else {
                    $this->setGeneralSearch($search, $fields);
                    $this->setOriginalSearch(
                        $query,
                        $modelTableName,
                        $fields,
                        $search,
                        $isFirstField,
                        $modelForceAndWhere
                    );
                }
            });
        }

        if (isset($orderBy) && !empty($orderBy)) {
            $split = StringUtil::explode('\|', $orderBy);

            if (count($split) > 1) {
                /*
                 * ex.
                 * products|description -> join products on current_table.product_id = products.id order by description
                 *
                 * products:
                 *      custom_id|products.description -> join products on current_table.custom_id = products.id order
                 * by products.description (in case both tables have same column name)
                 */
                $table = $model->getModel()->getTable();
                $sortTable = $split[0];
                $sortColumn = $split[1];

                $split = StringUtil::explode('\:', $sortTable);
                if (count($split) > 1) {
                    $sortTable = $split[0];
                    $keyName = $table . '.' . $split[1];
                } else {
                    /*
                     * If you do not define which column to use as a joining column on current table, it will
                     * use a singular of a join table appended with _id
                     *
                     * ex.
                     * products -> product_id
                     */
                    $prefix = str_singular($sortTable);
                    $keyName = $table . '.' . $prefix . '_id';
                }

                $model = $model
                    ->leftJoin($sortTable, $keyName, '=', $sortTable . '.id')
                    ->orderBy($sortColumn, $sortedBy)
                    ->addSelect($table . '.*');
            } else {
                $personalisedOrderBy = $this->getPersonalisedOrderBy($orderBy);
                // $orderBy != $personalisedOrderBy will prevent SQL injection
                if ($orderBy != $personalisedOrderBy && $this->hasSQLMethod($personalisedOrderBy)) {
                    if (preg_match('/(asc|desc)/i', $personalisedOrderBy)) {
                        $personalisedOrderBy = preg_replace(
                            '/(asc|desc)/i',
                            $sortedBy,
                            $personalisedOrderBy
                        );
                        $sortedBy = null;
                    }
                    $model = $model->orderByRaW($personalisedOrderBy . ' ' . $sortedBy);
                } elseif ($orderBy != $personalisedOrderBy) {
                    $model = $model->orderBy($personalisedOrderBy, $sortedBy);
                } else {
                    $model = $model->orderBy($orderBy, $sortedBy);
                }
            }
        }

        if (isset($filter) && !empty($filter)) {
            if (is_string($filter)) {
                $filter = StringUtil::explode('\;', $filter);
            }

            $model = $model->select($filter);
        }

        if ($with) {
            $with = StringUtil::explode('\;', $with);
            $model = $model->with($with);
        }

        return $model;
    }

    /**
     * @param array $search
     * @param array $fields
     */
    private function setGeneralSearch(&$search, &$fields)
    {
        if (!empty($search['general'])) {
            $general = $search['general'];
            $general = is_array($general) ? $general : [$general];
            unset($search['general']);

            foreach ($fields as $field => $condition) {
                if (is_numeric($field)) {
                    $field = $condition;
                }

                // Personalised fields should not be included in the search
                // since the names don't exist in any database table
                $personalisedFieldsSearchable = $this->getFieldNames(
                    $this->repository->getPersonalisedFieldsSearchable()
                );
                if (in_array($field, $personalisedFieldsSearchable)) {
                    continue;
                }

                if (!empty($search[$field])) {
                    $search[$field] = array_merge($search[$field], $general);
                } else {
                    $search[$field] = $general;
                }
            }
        }
    }

    /**
     * @param Builder $query
     * @param bool $isFirstField
     * @param array $value
     * @param $modelForceAndWhere
     * @param $relation
     * @param $modelTableName
     * @param string $field
     * @param string $condition
     * @throws WrongDateRangeException
     * @throws \ReflectionException
     */
    private function addFilter(
        &$query,
        &$isFirstField,
        $value,
        $modelForceAndWhere,
        $relation,
        $modelTableName,
        $field,
        $condition
    ) {
        foreach ($value as $item) {
            if (!empty($item) && ($isFirstField || $modelForceAndWhere)) {
                $this->setWhere($modelForceAndWhere, $query, $relation, $modelTableName, $field, $condition, $item);

                $isFirstField = false;
            } elseif (!empty($item)) {
                $this->setWhere($modelForceAndWhere, $query, $relation, $modelTableName, $field, $condition, $item);
            }
        }

        if ($isFirstField) {
            $isFirstField = false;
        }
    }

    /**
     * @param $modelForceAndWhere
     * @param Builder $query
     * @param $relation
     * @param $modelTableName
     * @param $field
     * @param $condition
     * @param $value
     * @throws WrongDateRangeException
     * @throws \ReflectionException
     */
    private function setWhere($modelForceAndWhere, &$query, $relation, $modelTableName, $field, $condition, $value)
    {
        $whereFunction = $modelForceAndWhere ? 'where' : 'orWhere';

        $personalisedField = $this->hasPersonalisedField($field, $value);
        if (is_array($personalisedField) && $condition !== 'between-date') {
            $value = StringUtil::sanitize($value);
            $query->orWhere(function ($query) use (
                $whereFunction,
                $personalisedField,
                $field,
                $condition,
                $value
            ) {
                foreach ($personalisedField as $key => $field) {
                    if (!is_numeric($key) && count($field) == 2) {
                        $query->$whereFunction($field[0], $field[1], $value);
                    } elseif (is_array($field) && count($field) == 3) {
                        $this->parseFieldValueToSql($field);
                        $this->setWhereRaw($query, $whereFunction . 'Raw', $field[0], $field[1], $field[2]);
                    } elseif (is_array($field) && count($field) == 4) {
                        $this->parseFieldValueToSql($field);
                        if ($field[3] == 'OR') {
                            $whereFunction = 'orWhere';
                        } elseif ($field[3] == 'AND') {
                            $whereFunction = 'where';
                        }
                        $this->setWhereRaw($query, $whereFunction . 'Raw', $field[0], $field[1], $field[2]);
                    } elseif ($key === 'raw') {
                        $query->orWhereRaw("$field[0]");
                    } else {
                        $this->setWhereRaw($query, $whereFunction . 'Raw', $field, $condition, $value);
                    }
                }
            });
        } elseif ((is_array($personalisedField) && $condition === 'between-date') ||
            $this->hasSQLMethod($field) ||
            $personalisedField !== $field
        ) {
            $field = ($personalisedField !== $field) ? $personalisedField : $field;

            $value = StringUtil::sanitize($value);
            $whereFunction .= 'Raw';
            $this->setWhereRaw($query, $whereFunction, $field, $condition, $value);
        } elseif (!is_null($relation)) {
            $query->$whereFunction(function ($subQuery) use ($relation, $field, $condition, $value) {
                $subQuery->where($relation . '.' . $field, $condition, $value);
                $subQuery->whereNotNull($relation . '.' . $field);
            });
        } else {
            $query->$whereFunction(function ($subQuery) use ($modelTableName, $field, $condition, $value) {
                $subQuery->where($modelTableName . '.' . $field, $condition, $value);
                $subQuery->whereNotNull($modelTableName . '.' . $field);
            });
        }
    }

    /**
     * @param Builder $query
     * @param $whereFunction
     * @param $field
     * @param $condition
     * @param $value
     * @throws WrongDateRangeException
     */
    private function setWhereRaw(&$query, $whereFunction, $field, $condition, $value)
    {
        if ($condition == 'in') {
            $or = strpos($whereFunction, 'or') === false;
            $whereFunction = $or ? 'orWhereIn' : 'whereIn';
            $query->$whereFunction($field, $value);
        } elseif (strpos($condition, 'between') !== false) {
            $value = explode('--', $value);
            $condition = StringUtil::replace('/between\-/i', '', $condition);

            $this->setValuesForInBetweenCondition($value, $condition);

            if (is_array($field) && count($field) == 2 && $condition === 'date') {
                $query->$whereFunction("{$field[0]} >= '{$value[0]}' AND {$field[1]} <= '{$value[1]}'");
            } else {
                $query->$whereFunction("$field BETWEEN '{$value[0]}' AND '{$value[1]}'");
            }
        } else {
            $value = $this->valueIsNullOrNumeric($value) ? $value : "'$value'";
            $query->$whereFunction("$field $condition $value");
        }
    }

    /**
     * @param $field
     * @param $condition
     */
    private function setFieldAndCondition(&$field, &$condition)
    {
        if (is_numeric($field)) {
            $field = $condition;
            $condition = "=";
        }

        $condition = trim(strtolower($condition));
    }

    /**
     * @param $field
     * @param $relation
     */
    private function setRelation(&$field, &$relation)
    {
        $relation = null;
        if (stripos($field, '.') !== false && !$this->hasSQLMethod($field)) {
            $explode = StringUtil::explode('\.', $field);
            $field = array_pop($explode);
            $relation = implode('.', $explode);
        }
    }

    /**
     * @param Builder $query
     * @param $modelTableName
     * @param $fields
     * @param $search
     */
    private function setCustomisedSearch(&$query, $modelTableName, $fields, $search)
    {
        foreach ($search as $fieldName => $searchValues) {
            if ($fieldName == 'general') {
                foreach ($searchValues as $searchValue) {
                    $this->setCustomGeneralSearch(
                        $query,
                        $modelTableName,
                        $fields,
                        $searchValue
                    );
                }
            } else {
                $this->setCustomFieldSearch(
                    $query,
                    $modelTableName,
                    $fieldName,
                    $fields,
                    $searchValues
                );
            }
        }
    }

    /**
     * @param Builder $query
     * @param $modelTableName
     * @param $fields
     * @param $search
     * @param $isFirstField
     * @param $modelForceAndWhere
     * @throws WrongDateRangeException
     * @throws \ReflectionException
     */
    private function setOriginalSearch(&$query, $modelTableName, $fields, $search, $isFirstField, $modelForceAndWhere)
    {
        foreach ($fields as $field => $condition) {
            $this->setFieldAndCondition($field, $condition);

            $value = [];

            if (!empty($search[$field])) {
                foreach ($search[$field] as $s) {
                    $value[] = ($condition == "like" || $condition == "ilike") ? "%{$s}%" : $s;
                }
            }

            $this->setRelation($field, $relation);

            $this->addFilter(
                $query,
                $isFirstField,
                $value,
                $modelForceAndWhere,
                $relation,
                $modelTableName,
                $field,
                $condition
            );
        }
    }

    /**
     * @param Builder $query
     * @param $modelTableName
     * @param $fields
     * @param $searchValue
     */
    private function setCustomGeneralSearch(
        &$query,
        $modelTableName,
        $fields,
        $searchValue
    ) {
        $query->orWhere(
            function ($subQuery) use ($modelTableName, $fields, $searchValue) {
                foreach ($fields as $field => $condition) {
                    if (is_numeric($field)) {
                        $field = $condition;
                        $condition = "=";
                    }

                    // Personalised fields should not be included in the search
                    // since the names don't exist in any database table
                    $personalisedFieldsSearchable = $this->getFieldNames(
                        $this->repository->getPersonalisedFieldsSearchable()
                    );

                    if (in_array($field, $personalisedFieldsSearchable)) {
                        continue;
                    }

                    $this->setRelation($field, $relation);

                    $value = ($condition == "like" || $condition == "ilike") ? "%{$searchValue}%" : $searchValue;

                    $this->setWhere(
                        false,
                        $subQuery,
                        $relation,
                        $modelTableName,
                        $field,
                        $condition,
                        $value
                    );
                }
            }
        );
    }

    /**
     * @param Builder $query
     * @param $modelTableName
     * @param $fieldName
     * @param $fields
     * @param $searchValues
     */
    private function setCustomFieldSearch(
        &$query,
        $modelTableName,
        $fieldName,
        $fields,
        $searchValues
    ) {
        $condition = '=';
        foreach ($fields as $field => $fieldCondition) {
            if ($field === $fieldName) {
                $condition = $fieldCondition;
                break;
            }
        }

        $query->where(
            function ($subQuery) use ($modelTableName, $fieldName, $searchValues, $condition) {
                $this->setRelation($fieldName, $relation);

                foreach ($searchValues as $searchValue) {
                    $value = ($condition == "like" || $condition == "ilike") ? "%{$searchValue}%" : $searchValue;

                    $this->setWhere(
                        false,
                        $subQuery,
                        $relation,
                        $modelTableName,
                        $fieldName,
                        $condition,
                        $value
                    );
                }
            }
        );
    }

    /**
     * @param $field
     * @return bool
     */
    private function hasSQLMethod($field)
    {
        switch ($field) {
            case (strpos($field, 'UPPER(') !== false):
            case (strpos($field, 'UPPER (') !== false):
            case (strpos($field, 'LOWER(') !== false):
            case (strpos($field, 'LOWER (') !== false):
            case (strpos($field, 'LENGTH(') !== false):
            case (strpos($field, 'LENGTH (') !== false):
            case (strpos($field, 'CONCAT(') !== false):
            case (strpos($field, 'CONCAT (') !== false):
            case (strpos($field, 'DATEDIFF') !== false):
            case (
                strpos($field, 'CASE WHEN') !== false &&
                strpos($field, 'THEN') !== false &&
                strpos($field, 'END') !== false
            ):
                return true;
            default:
                return false;
        }
    }

    /**
     * @param $field
     * @param $value
     * @return mixed
     * @throws \ReflectionException
     */
    private function hasPersonalisedField($field, $value)
    {
        $personalisedFieldsSearchable = $this->getFieldNames($this->repository->getPersonalisedFieldsSearchable());
        if (in_array($field, $personalisedFieldsSearchable)) {
            $filter = StringUtil::snakeToCamelCase($field) . 'Filter';
            if (method_exists($this->repository, $filter)) {
                $method = new \ReflectionMethod($this->repository, $filter);
                if (count($method->getParameters()) == 1) {
                    return $this->repository->$filter($value);
                }
                return $this->repository->$filter();
            }
        }

        return $field;
    }

    /**
     * @param array $searchArray
     * @throws NoSearchDependencyException
     */
    private function validateDependencies(array $searchArray)
    {
        $fields = array_keys($searchArray);
        $dependencies = $this->repository->getPersonalisedFieldsDependencies();

        foreach ($fields as $field) {
            foreach ($dependencies[$field] ?? [] as $dependency) {
                if (empty($searchArray[$dependency])) {
                    $message = 'When searching using the field ' .
                        $field .
                        ' you must also specify: ' .
                        implode(',', $dependencies[$field]);
                    throw new NoSearchDependencyException($message);
                }
            }
        }
    }

    protected function getFieldNames(array $fieldsArray)
    {
        $fields = [];
        foreach ($fieldsArray as $key => $item) {
            if (is_numeric($key)) {
                $fields[] = $item;
            } else {
                $fields[] = $key;
            }
        }

        return $fields;
    }

    /**
     * @param array $field
     */
    private function parseFieldValueToSql(array &$field)
    {
        if (in_array(count($field), [3, 4]) && $field[1] == '=' && $field[2] === null) {
            $field[1] = 'IS';
            $field[2] = 'NULL';
        } elseif (in_array(count($field), [3, 4]) && $field[1] == '!=' && $field[2] === null) {
            $field[1] = 'IS NOT';
            $field[2] = 'NULL';
        }
    }

    /**
     * @param string $orderBy
     * @return string
     */
    private function getPersonalisedOrderBy(string $orderBy)
    {
        $personalisedOrderBy = $this->repository->getPersonalisedOrderBy();

        $orderByMethod = StringUtil::snakeToCamelCase($orderBy) . 'OrderBy';
        if (in_array($orderBy, $personalisedOrderBy) && method_exists($this->repository, $orderByMethod)) {
            return $this->repository->$orderByMethod();
        }

        return $orderBy;
    }

    /**
     * @param $value
     * @return bool
     */
    private function valueIsNullOrNumeric($value)
    {
        return $value === 'NULL' || is_numeric($value);
    }
}
