<?php

namespace App\Criteria;

use Illuminate\Database\Query\Builder;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

class SortByCriteria implements CriteriaInterface
{
    /**
     * Column to sort by.
     *
     * @var string
     */
    public $column;

    /**
     * How to sort by.
     * e.g. ascending or descending.
     *
     * @var string
     */
    public $direction;

    /**
     * Initialise class.
     *
     * @param string $column
     * @param string $direction
     */
    public function __construct($column, $direction = 'asc')
    {
        $this->column    = $column;
        $this->direction = $direction;
    }

    /**
     * Apply criteria in query repository
     *
     * @param Builder             $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model->orderBy($this->column, $this->direction);

        return $model;
    }
}
