<?php

namespace App\Criteria;

use Illuminate\Database\Query\Builder;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

class LimitCriteria implements CriteriaInterface
{
    /**
     * @var integer
     */
    protected $limit;

    /**
     * Initialise class.
     *
     * @param integer $limit
     */
    public function __construct($limit = 1)
    {
        $this->limit = $limit;
    }

    /**
     * Apply criteria in query repository
     *
     * @param Builder             $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model->limit($this->limit);

        return $model;
    }
}
