<?php

namespace App\Exceptions;

/**
 * Class NoSearchDependencyException
 * @package App\Exceptions
 */
class NoSearchDependencyException extends HttpApiException
{
    /**
     * @param string     $message  The internal exception message
     * @param \Exception $previous The previous exception
     * @param int        $code     The internal exception code
     * @param array      $headers
     */
    public function __construct(
        string $message = 'The search contains fields that depend on other fields.',
        \Exception $previous = null,
        int $code = 0,
        array $headers = []
    ) {
        parent::__construct(400, $message, $previous, $headers, $code);
    }
}
