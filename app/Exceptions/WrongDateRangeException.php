<?php

namespace App\Exceptions;

/**
 * Class WrongDateRangeException
 * @package App\Exceptions
 */
class WrongDateRangeException extends HttpApiException
{
    /**
     * @param string     $message  The internal exception message
     * @param \Exception $previous The previous exception
     * @param int        $code     The internal exception code
     * @param array      $headers
     */
    public function __construct(
        string $message = 'Wrong date range entered',
        \Exception $previous = null,
        int $code = 0,
        array $headers = []
    ) {
        parent::__construct(400, $message, $previous, $headers, $code);
    }
}
