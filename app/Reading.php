<?php

namespace App;

use App\Collections\ReadingCollection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Carbon;

/**
 * Class Reading
 * @package App
 * @version February 23, 2019, 10:34 pm UTC
 *
 * @property integer $id
 * @property integer $client_id
 * @property Client $client
 * @property integer $energy_type_id
 * @property EnergyType $energyType
 * @property Carbon $date
 * @property double $reading
 */
class Reading extends Model
{
    use SoftDeletes;

    public $table = 'readings';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'client_id',
        'energy_type_id',
        'reading',
        'date',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'client_id' => 'integer',
        'energy_type_id' => 'integer',
        'date' => 'datetime',
        'reading' => 'double'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'client_id' => 'required',
        'energy_type_id' => 'required',
        'reading' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function client()
    {
        return $this->belongsTo(Client::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function energyType()
    {
        return $this->belongsTo(EnergyType::class);
    }

    /**
     * Create a new Eloquent Collection instance.
     *
     * @param  array $models
     *
     * @return ReadingCollection
     */
    public function newCollection(array $models = [])
    {
        return new ReadingCollection($models);
    }

    /**
     * @return array
     */
    public function toDetailsArray()
    {
        return (new ReadingCollection([$this]))
            ->toDetailsArray()[0];
    }
}
