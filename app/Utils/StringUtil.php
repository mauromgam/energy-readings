<?php

namespace App\Utils;

class StringUtil
{
    /**
     * @param string $delimiter
     * @param string $string
     * @return array[]|false|string[]
     */
    public static function explode(string $delimiter, string $string)
    {
        $pattern = '@' . $delimiter . '@';

        return preg_split($pattern, $string, null, PREG_SPLIT_NO_EMPTY);
    }

    /**
     * @param string $pattern
     * @param string $replacement
     * @param string $string
     * @return null|string|string[]
     */
    public static function replace(string $pattern, string $replacement, string $string)
    {
        return preg_replace($pattern, $replacement, $string);
    }

    /**
     * @param string $string
     *
     * @return string
     */
    public static function sanitize(string $string): string
    {
        return escapeshellcmd($string);
    }

    /**
     * @param string $string
     * @param bool $firstCharUpperCase
     * @return string
     */
    public static function dashSeparatedToCamelCase(string $string, bool $firstCharUpperCase = false): string
    {
        $string = str_replace('-', '', ucwords($string, '-'));
        if ($firstCharUpperCase) {
            return $string;
        }

        return lcfirst($string);
    }

    /**
     * @param string $string
     * @param bool $firstCharUpperCase
     * @param bool $replaceUnderscoreWithSpace
     * @return mixed|string
     */
    public static function snakeToCamelCase(
        string $string,
        bool $firstCharUpperCase = false,
        bool $replaceUnderscoreWithSpace = false
    ) {
        if ($replaceUnderscoreWithSpace) {
            $string = array_map(function ($value) {
                return ucfirst($value);
            }, explode('_', $string));
            $string = implode(' ', $string);
        } else {
            $string = str_replace('_', '', ucwords($string, '_'));
        }

        if ($firstCharUpperCase) {
            return $string;
        }

        return lcfirst($string);
    }

    /**
     * @param string $string
     * @return string
     */
    public static function spaceToSnake(string $string): string
    {
        return strtolower(str_replace(' ', '_', $string));
    }

    /**
     * @param array $stringArray
     * @return array
     */
    public static function spaceToSnakeArray(array $stringArray): array
    {
        foreach ($stringArray as &$string) {
            $string = self::spaceToSnake($string);
        }

        return $stringArray;
    }

    /**
     * @param  string $string
     * @return string
     */
    public static function camelCaseToDash(string $string): string
    {
        return strtolower(preg_replace('/[A-Z]/', '-$0', $string));
    }

    /**
     * @param string $string
     *
     * @return null|string|string[]
     */
    public static function removeTimeFromDate(string $string)
    {
        return preg_replace('/\ [0-9]{2}\:[0-9]{2}\:[0-9]{2}/i', '', $string);
    }
}
