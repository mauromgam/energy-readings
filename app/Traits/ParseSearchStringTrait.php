<?php

namespace App\Traits;

use App\Exceptions\WrongDateRangeException;
use App\Utils\StringUtil;
use Illuminate\Support\Carbon;

/**
 * Trait ParseSearchStringTrait
 *
 * @package App\Traits
 */
trait ParseSearchStringTrait
{
    /**
     * Improves the way the query string param 'search' is broke up into filters
     * Ex: search=client:ClientName|ClientEmail;reading_date:2018-01-01--2019-01-01
     *
     * The example above will search the readings of a Client that has
     * the (name LIKE ClientName OR the email LIKE ClientEmail) AND the reading_date is in BETWEEN 2018-01-01 AND 2019-01-01
     *
     * @param string $search
     *
     * @return array
     */
    protected function parserSearchValue($search)
    {
        $phrases = [];
        if (stripos($search, ';') !== false ||
            stripos($search, ':') !== false ||
            stripos($search, '|') !== false
        ) {
            $values = StringUtil::explode('\;', $search);

            if (!$values) {
                return [];
            }

            foreach ($values as $value) {
                $stringArray = StringUtil::explode('\:', $value);
                if (!empty($stringArray[0]) && strpos($stringArray[0], ' ') !== false) {
                    $phrases['general'][] = $value;
                } else {
                    $this->pushPhrases($phrases, $stringArray);
                }
            }
        } elseif ($search) {
            $phrases['general'][] = $search;
        }

        return $phrases;
    }

    /**
     * This method will separate general queries from specific queries
     *
     * Ex: search=client:ClientName|ClientEmail;some random search
     * For the example above, the result array will be:
     *      [
     *          'general' => 'some random search',
     *          'client'  => [
     *              'ClientName',
     *              'ClientEmail,
     *          ]
     *      ]
     *
     * The value assigned to the 'general' key will search using every column
     * specified in the MyBaseRepository::$fieldSearchable property.
     * The values assigned to the 'client' key will search using a personalised search defined on
     * MyBaseRepository::$personalisedFieldSearchable which requires a method, in this case, called clientFilter(),
     * such as ReadingRepository::clientFilter().
     *
     * @param array $phrases
     * @param array $stringArray
     */
    public function pushPhrases(&$phrases, $stringArray): void
    {
        if (count($stringArray) == 2) {
            if (stripos($stringArray[1], '|') !== false) {
                $phrases = array_merge(
                    [$stringArray[0] => StringUtil::explode('\|', $stringArray[1])],
                    $phrases
                );
            } elseif (!empty($phrases[$stringArray[0]])) {
                $phrases[$stringArray[0]] = array_merge(
                    $phrases[$stringArray[0]],
                    StringUtil::explode('\|', $stringArray[1])
                );
            } else {
                $phrases[$stringArray[0]] = StringUtil::explode('\|', $stringArray[1]);
            }
        } elseif (count($stringArray) == 1) {
            if (stripos($stringArray[0], '|') !== false) {
                if (empty($phrases['general'])) {
                    $phrases['general'] = StringUtil::explode('\|', $stringArray[0]);
                } else {
                    $phrases['general'] = array_merge(StringUtil::explode('\|', $stringArray[0]), $phrases['general']);
                }
            } else {
                $phrases['general'][] = $stringArray[0];
            }
        }
    }

    /**
     * Sets the values when the search uses between-integer or between-date where '--' is the delimiter that
     * splits both limits.
     * If one of the limits is missing, depending on the $condition value:
     *      - when first limit is not provided, the search will use the Unix epoch date or 0
     *      - when the second limit is not provided, the search will user the current date + 200 years or 1000000
     * Formats:
     *      2018-01-01--2019-01-01 OR 2018-01-01-- OR --2019-01-01
     *      0--10 OR 0-- OR --10
     *
     * @param array $value
     * @param string $condition
     * @throws WrongDateRangeException
     */
    public function setValuesForInBetweenCondition(array &$value, string $condition): void
    {
        $substitute = null;
        if ($condition == 'integer' && empty($value[0])) {
            $value[0] = 0;
        } elseif ($condition == 'date' && empty($value[0])) {
            $value[0] = '1970-01-01 00:00:00';
        } elseif ($condition == 'integer' && empty($value[1])) {
            $value[1] = 1000000;
        } elseif ($condition == 'date' && empty($value[1])) {
            $value[1] = Carbon::now()->addYears(200)->__toString();
        }
        $this->setDateToBeginningOrEndOfDay($condition, $value);
        $this->validateRange($value);
    }

    /**
     * @param string $condition
     * @param array $value
     */
    protected function setDateToBeginningOrEndOfDay(string $condition, array &$value): void
    {
        if ($condition == 'date') {
            $pattern = "/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1]) " .
                "([0-1][0-9]|2[0-3])\:([0-5][0-9])\:([0-5][0-9])$/";
            if (!preg_match($pattern, $value[0])) {
                $value[0] .= ' 00:00:00';
            }

            if (!preg_match($pattern, $value[1])) {
                $value[1] .= ' 23:59:59';
            }
        }
    }

    /**
     * Validates the range for either a date or integer range
     *
     * @param array $value
     * @throws WrongDateRangeException
     */
    protected function validateRange(array $value): void
    {
        if ($value[0] > $value[1]) {
            throw new WrongDateRangeException();
        }
    }
}
