<?php

namespace App;

use App\Collections\ClientCollection;
use App\Collections\ReadingCollection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Carbon;

/**
 * Class Client
 * @package App
 * @version February 23, 2019, 9:25 pm UTC
 *
 * @property integer $id
 * @property string $account_number
 * @property string $first_name
 * @property string $last_name
 * @property string $phone_number
 * @property string $email
 * @property Carbon $default_reading_date
 * @property boolean $is_enabled
 * @property ReadingCollection $readings
 */
class Client extends Model
{
    use SoftDeletes;

    public $table = 'clients';
    
    protected $dates = ['deleted_at'];

    public $fillable = [
        'account_number',
        'first_name',
        'last_name',
        'phone_number',
        'email',
        'is_enabled',
        'default_reading_date'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'account_number' => 'string',
        'first_name' => 'string',
        'last_name' => 'string',
        'phone_number' => 'string',
        'email' => 'string',
        'is_enabled' => 'boolean',
        'default_reading_date' => 'date',
 ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'account_number' => 'required',
        'first_name' => 'required|string',
        'last_name' => 'required|string',
        'email' => 'required|email',
        'default_reading_date' => 'required|date'
    ];

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->first_name . ' ' . $this->last_name;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function readings()
    {
        return $this->hasMany(Reading::class);
    }

    /**
     * Create a new Eloquent Collection instance.
     *
     * @param  array $models
     *
     * @return ClientCollection
     */
    public function newCollection(array $models = [])
    {
        return new ClientCollection($models);
    }

    /**
     * @return array
     */
    public function toDetailsArray()
    {
        return (new ClientCollection([$this]))
            ->toDetailsArray()[0];
    }
}
