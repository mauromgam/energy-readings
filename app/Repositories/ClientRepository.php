<?php

namespace App\Repositories;

use App\Client;
use Illuminate\Database\Query\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

/**
 * Class ClientRepository
 * @package App\Repositories
 * @version February 23, 2019, 9:25 pm UTC
 *
 * @method Client findWithoutFail($id, $columns = ['*'])
 * @method Client find($id, $columns = ['*'])
 * @method Client first($columns = ['*'])
 */
class ClientRepository extends MyBaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'account_number' => 'like',
        'first_name' => 'like',
        'last_name' => 'like',
        'email' => 'like',
        'phone_number',
        'default_reading_date',
    ];

    /**
     * @var array
     */
    protected $personalisedFieldSearchable = [
        'reading_date' => 'between-date',
    ];

    /**
     * @return string
     */
    public function readingDateFilter(): string
    {
        return 'DATE(readings.date)';
    }

    /**
     * Configure the Model
     *
     * @return string
     */
    public function model(): string
    {
        return Client::class;
    }

    /**
     * Returns all the clients
     *
     * @param Model|Builder $client
     * @param Request $request
     * @param array $columns
     * @return LengthAwarePaginator
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function getAllPaginated($client, Request $request, $columns = ['*']): LengthAwarePaginator
    {
        $this->applyCriteriaToModel($client, $request);

        return parent::getAllPaginated($client, $request, $columns);
    }
}
