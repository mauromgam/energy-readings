<?php

namespace App\Repositories;

use App\EnergyType;
use Illuminate\Database\Query\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

/**
 * Class EnergyTypeRepository
 * @package App\Repositories
 * @version February 22, 2019, 7:20 pm UTC
 *
 * @method EnergyType findWithoutFail($id, $columns = ['*'])
 * @method EnergyType find($id, $columns = ['*'])
 * @method EnergyType first($columns = ['*'])
 */
class EnergyTypeRepository extends MyBaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name' => 'like'
    ];

    /**
     * Configure the Model
     * @return string
     */
    public function model(): string
    {
        return EnergyType::class;
    }

    /**
     * Returns all Energy Types
     *
     * @param Model|Builder $energyType
     * @param Request $request
     * @param array $columns
     * @return LengthAwarePaginator
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function getAllPaginated($energyType, Request $request, $columns = ['*']): LengthAwarePaginator
    {
        $this->applyCriteriaToModel($energyType, $request);

        return parent::getAllPaginated($energyType, $request, $columns);
    }
}
