<?php

namespace App\Repositories;

use \Illuminate\Database\Eloquent\Model;
use \Illuminate\Http\Request;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

interface MyRepositoryInterface
{
    public function findWithoutFail($id, $columns = ['*']): ?Model;
    public function applyCriteriaToModel(Model &$model, Request $request): void;
    public function transformBasicCollection(LengthAwarePaginator &$paginatedResults): void;
    public function getAllPaginated($reading, Request $request, $columns = ['*']): LengthAwarePaginator;
}
