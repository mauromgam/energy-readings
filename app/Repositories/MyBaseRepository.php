<?php

namespace App\Repositories;

use App\Criteria\MyRequestCriteria;
use App\Traits\ParseSearchStringTrait;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;
use InfyOm\Generator\Common\BaseRepository;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Contracts\CriteriaInterface;

abstract class MyBaseRepository extends BaseRepository implements MyRepositoryInterface
{
    use ParseSearchStringTrait;

    const ITEMS_PER_PAGE = 20;

    protected $personalisedFieldSearchable = [];
    protected $personalisedFieldDependency = [];
    protected $personalisedOrderBy = [];

    /**
     * @return array
     */
    public function getFieldsSearchable(): array
    {
        return $this->fieldSearchable;
    }

    /**
     * @return array
     */
    public function getPersonalisedFieldsSearchable(): array
    {
        return $this->personalisedFieldSearchable;
    }

    /**
     * @return array
     */
    public function getPersonalisedFieldsDependencies(): array
    {
        return $this->personalisedFieldDependency;
    }

    /**
     * @return array
     */
    public function getPersonalisedOrderBy(): array
    {
        return $this->personalisedOrderBy;
    }

    /**
     * @param string $id
     * @param array $columns
     *
     * @return Model|null
     */
    public function findWithoutFail($id, $columns = ['*']): ?Model
    {
        try {
            return $this->find($id, $columns);
        } catch (Exception $e) {
            Log::error($e);

            return null;
        }
    }

    /**
     * @param Model $model
     * @param Request $request
     *
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function applyCriteriaToModel(Model &$model, Request $request): void
    {
        $this->pushCriteria(new MyRequestCriteria($request));
        $this->pushCriteria(new LimitOffsetCriteria($request));

        $criteria = $this->getCriteria();
        foreach ($criteria as $c) {
            if ($c instanceof CriteriaInterface) {
                $model = $c->apply($model, $this);
            }
        }
    }

    /**
     * @param \Illuminate\Database\Query\Builder $model
     * @param Request $request
     * @param array $columns
     * @return LengthAwarePaginator
     */
    public function getAllPaginated($model, Request $request, $columns = ['*']): LengthAwarePaginator
    {
        $page = isset($request->page) ? $request->page : 1;
        $model = $model->select($columns);

        $results = $model
            ->paginate($request->get('page_size') ?? self::ITEMS_PER_PAGE, ['*'], 'page', $page);
        $this->transformBasicCollection($results);

        return $results;
    }

    /**
     * @param LengthAwarePaginator $paginatedResults
     */
    public function transformBasicCollection(LengthAwarePaginator &$paginatedResults): void
    {
        $collection = $paginatedResults->getCollection();
        $arrayCollection = Collection::make($collection->toBasicArray());
        $paginatedResults->setCollection($arrayCollection);
    }
}
