<?php

namespace App\Repositories;

use App\Collections\ReadingCollection;
use App\Reading;
use Illuminate\Database\Query\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

/**
 * Class ReadingRepository
 * @package App\Repositories
 * @version February 23, 2019, 10:34 pm UTC
 *
 * @method Reading findWithoutFail($id, $columns = ['*'])
 * @method Reading find($id, $columns = ['*'])
 * @method Reading first($columns = ['*'])
 */
class ReadingRepository extends MyBaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'reading',
    ];

    protected $personalisedFieldSearchable = [
        'client' => 'like',
        'energy_type',
        'date' => 'between-date',
    ];

    /**
     * @return array
     */
    public function clientFilter(): array
    {
        return [
            'clients.first_name',
            'clients.last_name',
            'clients.email',
            'CONCAT(clients.first_name, \' \', clients.last_name)'
        ];
    }

    /**
     * @param string $energyType
     * @return array
     */
    public function energyTypeFilter(string $energyType): array
    {
        $energyType = strtolower($energyType);
        return [
            ['LOWER(energy_types.name)', '=', $energyType]
        ];
    }

    /**
     * @return string
     */
    public function dateFilter(): string
    {
        return 'DATE(readings.date)';
    }

    /**
     * Configure the Model
     **/
    public function model(): string
    {
        return Reading::class;
    }

    /**
     * @param Model|Builder $reading
     * @param Request $request
     * @param array $columns
     * @return LengthAwarePaginator
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function getAllPaginated($reading, Request $request, $columns = ['*']): LengthAwarePaginator
    {
        $this->generateQuery($reading, $request);

        return parent::getAllPaginated($reading, $request, $columns);
    }

    /**
     * @param Model $reading
     * @param Request $request
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function generateQuery(Model &$reading, Request $request): void
    {
        $this->applyCriteriaToModel($reading, $request);

        $reading->with(['client', 'energyType']);
        $reading = $reading->join('clients', 'clients.id', '=', 'readings.client_id')
            ->join('energy_types', 'energy_types.id', '=', 'readings.energy_type_id');
    }

    /**
     * Returns all the readings belonging to a client
     *
     * @param integer $clientId
     * @param Reading $reading
     * @param Request $request
     * @return ReadingCollection
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function getClientReadings($clientId, Reading $reading, Request $request): ReadingCollection
    {
        $this->generateQuery($reading, $request);
        $readings = $reading->where('clients.id', '=', $clientId)
            ->get(['readings.*']);

        return $readings;
    }
}
