<?php

namespace App\Http\Middleware;

use App\User;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class LoginProxy
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $clientName = env('APP_NAME');

        $client = DB::table('oauth_clients')
            ->where([
                ['name', $clientName],
                ['password_client', true]
            ])
            ->first();

        if (!$client) {
            Log::error("Could not find OAuth client $clientName");

            return response()->json([
                'success' => false,
                'message' => 'Could not authenticate',
            ], 500);
        }

        $refreshToken = $request->input('refresh_token');

        $request->merge([
            'grant_type' => $refreshToken ? 'refresh_token' : 'password',
            'client_id' => $client->id,
            'client_secret' => $client->secret,
            'scope' => '',
        ]);

        if ($refreshToken) {
            return $this->authRefreshToken($request, $next);
        }

        return $this->authPassword($request, $next);
    }

    /**
     * Password authentication.
     *
     * @param Request $request
     * @param Closure $next
     * @return \Illuminate\Http\JsonResponse|mixed
     */
    protected function authPassword($request, Closure $next)
    {
        $response = $next($request);

        /** @var User|null $user */
        $user = User::where('email', $request->input('username'))
            ->where(function ($query) {
                $query->where('is_enabled', true);
            })
            ->first();

        if (!$user) {
            return response()->json(['error' => 'user_locked'], 403);
        }

        if ($this->maxLoginAttempts($response, $user)) {
            return response()->json(['error' => 'max_failed_attempts'], 403);
        }

        return $response;
    }

    /**
     * Refresh Token.
     *
     * @param  Request $request
     * @param  Closure $next
     * @return Request
     */
    protected function authRefreshToken($request, Closure $next)
    {
        return $next($request);
    }

    /**
     * @param Response $response
     * @param User $user
     * @return bool
     */
    public function maxLoginAttempts($response, User $user)
    {
        $responseContent = json_decode($response->content());

        $maxFailedLoginAttempts = (int)env('MAX_FAILED_LOGIN_ATTEMPTS');

        if (($responseContent->error ?? null) === "invalid_credentials") {
            if ($user->failed_login_attempts < $maxFailedLoginAttempts) { // do not increment past max
                $user->increment('failed_login_attempts');
            }
        } elseif ($user->failed_login_attempts > 0) {
            if ($user->failed_login_attempts !== $maxFailedLoginAttempts) { // not already locked
                $user->failed_login_attempts = 0; // reset
                $user->save();
            }
        }
        if ($user->failed_login_attempts >= $maxFailedLoginAttempts) { // account locked until pwd reset
            return true;
        }

        return false;
    }
}
