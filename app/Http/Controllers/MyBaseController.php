<?php

namespace App\Http\Controllers;

use App\Utils\ResponseUtil;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Response;
use InfyOm\Generator\Controller\AppBaseController;

/**
 * Class MyBaseController
 * @package App\Http\Controllers
 */
class MyBaseController extends AppBaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected $repository;

    /**
     * @param Model|LengthAwarePaginator|array $result
     * @param string $message
     * @param int $status
     *
     * @return JsonResponse
     */
    public function sendResponse($result, $message, $status = 200): JsonResponse
    {
        if ($result instanceof Model) {
            $result = $result->toArray();
        }

        return Response::json(ResponseUtil::makeResponse($message, $result), $status);
    }

    /**
     * @param string $error
     * @param int $status
     * @param array $data
     *
     * @return JsonResponse
     */
    public function sendError($error, $status = 400, $data = []): JsonResponse
    {
        return Response::json(ResponseUtil::makeError($error, $data), $status);
    }
}
