<?php

namespace App\Http\Controllers;

use App\Client;
use App\Criteria\MyRequestCriteria;
use App\Http\Requests\CreateReadingAPIRequest;
use App\Http\Requests\UpdateReadingAPIRequest;
use App\Reading;
use App\Repositories\ReadingRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;

/**
 * Class ReadingAPIController
 * @package App\Http\Controllers
 */
class ReadingAPIController extends MyBaseController
{
    /** @var  ReadingRepository */
    protected $readingRepository;

    public function __construct(ReadingRepository $readingRepo)
    {
        $this->readingRepository = $readingRepo;
    }

    /**
     * Display a listing of the Reading.
     * GET|HEAD /readings
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function index(Request $request): JsonResponse
    {
        $this->readingRepository->pushCriteria(new MyRequestCriteria($request));
        $this->readingRepository->pushCriteria(new LimitOffsetCriteria($request));
        $readings = $this->readingRepository->getAllPaginated(new Reading(), $request, ['readings.*']);

        return $this->sendResponse($readings, 'Readings retrieved successfully');
    }

    /**
     * Store a newly created Reading in storage.
     * POST /readings
     *
     * @param CreateReadingAPIRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function store(CreateReadingAPIRequest $request): JsonResponse
    {
        $input = $request->all();

        $readings = $this->readingRepository->create($input);

        return $this->sendResponse($readings->toArray(), 'Reading saved successfully');
    }

    /**
     * Display the specified Reading.
     * GET|HEAD /readings/{id}
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id): JsonResponse
    {
        /** @var Reading $reading */
        $reading = $this->readingRepository->findWithoutFail($id);

        if (empty($reading)) {
            return $this->sendError('Reading not found', 404);
        }

        return $this->sendResponse($reading->toDetailsArray(), 'Reading retrieved successfully');
    }

    /**
     * Display the specified Client Readings.
     * GET|HEAD /clients/{id}/readings
     *
     * @param  int $id
     * @param Request $request
     *
     * @return JsonResponse
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function getClientReadings($id, Request $request): JsonResponse
    {
        /** @var Client $clients */
        $client = Client::where('id', '=', $id)->first();
        if (empty($client)) {
            return $this->sendError('Client not found', 404);
        }

        $readings = $this->readingRepository->getClientReadings($client->id, new Reading(), $request);

        return $this->sendResponse($readings->toBasicArray(), 'Client readings retrieved successfully');
    }

    /**
     * Update the specified Reading in storage.
     * PUT/PATCH /readings/{id}
     *
     * @param  int $id
     * @param UpdateReadingAPIRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function update($id, UpdateReadingAPIRequest $request): JsonResponse
    {
        $input = $request->all();

        /** @var Reading $reading */
        $reading = $this->readingRepository->findWithoutFail($id);

        if (empty($reading)) {
            return $this->sendError('Reading not found', 404);
        }

        $reading = $this->readingRepository->update($input, $id);

        return $this->sendResponse($reading->toArray(), 'Reading updated successfully');
    }

    /**
     * Remove the specified Reading from storage.
     * DELETE /readings/{id}
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy($id): JsonResponse
    {
        /** @var Reading $reading */
        $reading = $this->readingRepository->findWithoutFail($id);

        if (empty($reading)) {
            return $this->sendError('Reading not found', 404);
        }

        $reading->delete();

        return $this->sendResponse($reading, 'Reading deleted successfully');
    }
}
