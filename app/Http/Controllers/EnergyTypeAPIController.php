<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateEnergyTypeAPIRequest;
use App\Http\Requests\UpdateEnergyTypeAPIRequest;
use App\EnergyType;
use App\Repositories\EnergyTypeRepository;
use Illuminate\Http\Request;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use App\Criteria\MyRequestCriteria;

/**
 * Class EnergyTypeAPIController
 * @package App\Http\Controllers
 */

class EnergyTypeAPIController extends MyBaseController
{
    /** @var  EnergyTypeRepository */
    protected $energyTypeRepository;

    public function __construct(EnergyTypeRepository $energyTypeRepo)
    {
        $this->energyTypeRepository = $energyTypeRepo;
    }

    /**
     * Display a listing of the EnergyType.
     * GET|HEAD /energyTypes
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function index(Request $request)
    {
        $this->energyTypeRepository->pushCriteria(new MyRequestCriteria($request));
        $this->energyTypeRepository->pushCriteria(new LimitOffsetCriteria($request));
        $energyTypes = $this->energyTypeRepository->getAllPaginated(new EnergyType(), $request);

        return $this->sendResponse($energyTypes, 'Energy Types retrieved successfully');
    }

    /**
     * Store a newly created EnergyType in storage.
     * POST /energyTypes
     *
     * @param CreateEnergyTypeAPIRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function store(CreateEnergyTypeAPIRequest $request)
    {
        $input = $request->all();

        $energyType = $this->energyTypeRepository->create($input);

        return $this->sendResponse($energyType->toArray(), 'Energy Type saved successfully');
    }

    /**
     * Display the specified EnergyType.
     * GET|HEAD /energyTypes/{id}
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        /** @var EnergyType $energyType */
        $energyType = $this->energyTypeRepository->findWithoutFail($id);

        if (empty($energyType)) {
            return $this->sendError('Energy Type not found', 404);
        }

        return $this->sendResponse($energyType->toDetailsArray(), 'Energy Type retrieved successfully');
    }

    /**
     * Update the specified EnergyType in storage.
     * PUT/PATCH /energyTypes/{id}
     *
     * @param  int $id
     * @param UpdateEnergyTypeAPIRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function update($id, UpdateEnergyTypeAPIRequest $request)
    {
        $input = $request->all();

        /** @var EnergyType $energyType */
        $energyType = $this->energyTypeRepository->findWithoutFail($id);

        if (empty($energyType)) {
            return $this->sendError('Energy Type not found', 404);
        }

        $energyType = $this->energyTypeRepository->update($input, $id);

        return $this->sendResponse($energyType->toArray(), 'EnergyType updated successfully');
    }

    /**
     * Remove the specified EnergyType from storage.
     * DELETE /energyTypes/{id}
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy($id)
    {
        /** @var EnergyType $energyType */
        $energyType = $this->energyTypeRepository->findWithoutFail($id);

        if (empty($energyType)) {
            return $this->sendError('Energy Type not found', 404);
        }

        $energyType->delete();

        return $this->sendResponse($energyType, 'Energy Type deleted successfully');
    }
}
