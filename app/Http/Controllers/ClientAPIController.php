<?php

namespace App\Http\Controllers;

use App\Client;
use App\Criteria\MyRequestCriteria;
use App\Http\Requests\CreateClientAPIRequest;
use App\Http\Requests\UpdateClientAPIRequest;
use App\Repositories\ClientRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;

/**
 * Class ClientAPIController
 * @package App\Http\Controllers
 */
class ClientAPIController extends MyBaseController
{
    /** @var  ClientRepository $clientRepository */
    protected $clientRepository;

    /**
     * ClientAPIController constructor.
     * @param ClientRepository $clientRepository
     */
    public function __construct(ClientRepository $clientRepository)
    {
        $this->clientRepository = $clientRepository;
    }

    /**
     * Display a listing of the Clients.
     * GET|HEAD /clients
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function index(Request $request): JsonResponse
    {
        $this->clientRepository->pushCriteria(new MyRequestCriteria($request));
        $this->clientRepository->pushCriteria(new LimitOffsetCriteria($request));
        $clients = $this->clientRepository->getAllPaginated(new Client(), $request);

        return $this->sendResponse($clients, 'Client retrieved successfully');
    }

    /**
     * Store a newly created Client in storage.
     * POST /clients
     *
     * @param CreateClientAPIRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function store(CreateClientAPIRequest $request): JsonResponse
    {
        $input = $request->all();

        $client = $this->clientRepository->create($input);

        return $this->sendResponse($client->toArray(), 'Client saved successfully');
    }

    /**
     * Display the specified Client.
     * GET|HEAD /clients/{id}
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id): JsonResponse
    {
        /** @var Client $client */
        $client = $this->clientRepository->findWithoutFail($id);

        if (empty($client)) {
            return $this->sendError('Client not found', 404);
        }

        return $this->sendResponse($client->toDetailsArray(), 'Client retrieved successfully');
    }

    /**
     * Update the specified Client in storage.
     * PUT/PATCH /clients/{id}
     *
     * @param  int $id
     * @param UpdateClientAPIRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function update($id, UpdateClientAPIRequest $request): JsonResponse
    {
        $input = $request->all();

        /** @var Client $client */
        $client = $this->clientRepository->findWithoutFail($id);

        if (empty($client)) {
            return $this->sendError('Client not found', 404);
        }

        $client = $this->clientRepository->update($input, $id);

        return $this->sendResponse($client->toArray(), 'Client updated successfully');
    }

    /**
     * Remove the specified Client from storage.
     * DELETE /clients/{id}
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy($id): JsonResponse
    {
        /** @var Client $client */
        $client = $this->clientRepository->findWithoutFail($id);

        if (empty($client)) {
            return $this->sendError('Client not found', 404);
        }

        $client->delete();

        return $this->sendResponse($client, 'Client deleted successfully');
    }
}
