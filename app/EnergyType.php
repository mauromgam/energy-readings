<?php

namespace App;

use App\Collections\EnergyTypeCollection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class EnergyType
 * @package App
 * @version February 22, 2019, 7:20 pm UTC
 *
 * @property integer $id
 * @property string $name
 * @property boolean $is_enabled
 */
class EnergyType extends Model
{
    use SoftDeletes;

    public $table = 'energy_types';

    protected $dates = ['deleted_at'];

    public $fillable = [
        'name',
        'is_enabled'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'name' => 'string',
        'is_enabled' => 'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required|string'
    ];

    /**
     * Create a new Eloquent Collection instance.
     *
     * @param  array $models
     *
     * @return EnergyTypeCollection
     */
    public function newCollection(array $models = [])
    {
        return new EnergyTypeCollection($models);
    }

    /**
     * @return array
     */
    public function toDetailsArray()
    {
        return (new EnergyTypeCollection([$this]))
            ->toBasicArray()[0];
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function readings()
    {
        return $this->hasMany(Reading::class);
    }
}
