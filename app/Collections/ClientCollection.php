<?php

namespace App\Collections;

use App\Client;
use Illuminate\Database\Eloquent\Collection;

/**
 * Class ClientCollection
 * @package App\Collections
 * @method Client first(callable $callback = null, $default = null)
 */
class ClientCollection extends Collection implements MyCollectionInterface
{
    /**
     * @return array
     */
    public function toBasicArray(): array
    {
        return $this->map(function (Client $client) {
            return [
                'id' => $client->id,
                'name' => $client->first_name . ' ' . $client->last_name,
                'email' => $client->email,
                'is_enabled' => $client->is_enabled,
            ];
        })->all();
    }

    /**
     * @return array
     */
    public function toDetailsArray(): array
    {
        return $this->map(function (Client $client) {
            return [
                'id' => $client->id,
                'first_name' => $client->first_name,
                'last_name' => $client->last_name,
                'email' => $client->email,
                'phone_number' => $client->phone_number,
                'account_number' => $client->account_number,
                'default_reading_date' => $client->default_reading_date->toDateString(),
                'is_enabled' => $client->is_enabled,
            ];
        })->all();
    }
}
