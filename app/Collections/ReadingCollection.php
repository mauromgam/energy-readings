<?php

namespace App\Collections;

use App\Reading;
use Illuminate\Database\Eloquent\Collection;

/**
 * Class ReadingCollection
 * @package App\Collections
 * @method Reading first(callable $callback = null, $default = null)
 */
class ReadingCollection extends Collection implements MyCollectionInterface
{
    /**
     * @return array
     */
    public function toBasicArray(): array
    {
        return $this->map(function (Reading $reading) {
            return [
                'id' => $reading->id,
                'client_name' => $reading->client->getName(),
                'energy_type_name' => $reading->energyType->name,
                'reading' => $reading->reading,
                'date' => $reading->date->toDateTimeString(),
            ];
        })->all();
    }

    /**
     * @return array
     */
    public function toDetailsArray(): array
    {
        return $this->map(function (Reading $reading) {
            return [
                'id' => $reading->id,
                'client' => $reading->client->toDetailsArray(),
                'energy_type_name' => $reading->energyType->name,
                'reading' => $reading->reading,
                'date' => $reading->date->toDateTimeString(),
            ];
        })->all();
    }
}
