<?php

namespace App\Collections;

interface MyCollectionInterface
{
    public function toBasicArray(): array;
}
