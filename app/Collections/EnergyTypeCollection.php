<?php

namespace App\Collections;

use App\EnergyType;
use Illuminate\Database\Eloquent\Collection;

/**
 * Class EnergyTypeCollection
 * @package App\Collections
 * @method EnergyType first(callable $callback = null, $default = null)
 */
class EnergyTypeCollection extends Collection implements MyCollectionInterface
{
    /**
     * @return array
     */
    public function toBasicArray(): array
    {
        return $this->map(function (EnergyType $energyType) {
            return [
                'id' => $energyType->id,
                'name' => $energyType->name,
                'is_enabled' => $energyType->is_enabled,
            ];
        })->all();
    }
}
