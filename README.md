# Simple Energy Readings CRUD

This is the implementation of an API that will execute the CRUD actions
for a simplified version of a system that manages clients energy readings.

To implement this API I used the following packages:

- "infyomlabs/laravel-generator": "5.7.x-dev"
    - this package helps on generating the first files by creating the Controller, 
    Model and Repository files. You can find how to use it in 
    http://labs.infyom.com/laravelgenerator/docs/5.7/introduction
    - you can find the JSON files to generate the classes in `resources/model_schemas`
    - some improvement were made on the RequestCriteria and BaseRepository to work
    better when using the search. To kow more about how to use the search please see
    https://github.com/andersao/l5-repository#using-the-requestcriteria
    - the changes made to the RequestCriteria and BaseRepository are explained on
    \App\Traits\ParseSearchStringTrait, \App\Criteria\MyRequestCriteria and 
    \App\Repositories\MyBaseRepositories
    
- "laravel/passport": "^7.2"

- "phpstan/phpstan": "^0.11.2"
    - PHP Static Analysis Tool



- Installation
    - `composer install`
    - `cp .env.example .env`
    - `php artisan passport:install`
    - `php artisan key:generate`
    - Set up the database config in `.env`
    - `php artisan migrate:fresh --seed`


    
- Run tests
    - `vendor/bin/phpunit tests/`

- Login credentials
    
    `User: test.user@test.com`
    
    `Passwrod: secret`



- Calling the API

    `GET/POST - http://localhost:PORT/api/v1/energy-types`

    `GET/PUT/DELETE - http://localhost:PORT/api/v1/energy-types/{energyTypeID}`
    
    - POST and PUT body:

            {
        
        	    "name": "Gas"
            
                "is_enabled": true
            
            }

    `GET/POST - http://localhost:PORT/api/v1/clients`

    `GET/PUT/DELETE - http://localhost:PORT/api/v1/clients/{clientID}`
    
    - POST and PUT body:

            {
        
        	    "first_name": "C",
            
        	    "last_name": "LastName 12345678",
            
                "email": "test.client@hotmail.com",
            
                "phone_number": "(123) 456-7890",
            
                "account_number": "987656789",
            
                "reading_date": "2019-02-23",
            
                "is_enabled": true
            
            }
    
    `GET - http://localhost:PORT/api/v1/clients/{clientId}/readings`
    
    `GET/POST - http://localhost:PORT/api/v1/readings`
    
    `GET/PUT/DELETE - http://localhost:PORT/api/v1/readings/{readingId}`

    - POST and PUT body:

            {
        
        	    "client_id": 1,
            
        	    "energy_type_id": 1,
            
                "reading": "123456.7890",
                
                "date": "2019-02-23"
            
            }



- Using the search:
    
    - Client example: `GET - http://localhost:PORT/api/v1/clients?search=ClientName|ClientEmail;account_number:12345`
    
        - The example above will generate a query like this: 
        
                "select
                
                    * 
                
                from
                
                    clients 
            
                where 
            
                    (
                    
                            first_name LIKE '%ClientName%' 
                    
                            OR first_name LIKE '%ClientEmail%' 
                            
                            OR email LIKE '%ClientName%' 
                            
                            OR email LIKE '%ClientEmail%' 
                            
                            ... 
                            
                    ) -- it will search in all the searchable fields in the clients table using both ClientName and ClientEmail

                    AND account_number = '12345';"
