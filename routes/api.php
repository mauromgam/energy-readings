<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::middleware('login.proxy')
    ->post('user/login', '\Laravel\Passport\Http\Controllers\AccessTokenController@issueToken');

Route::middleware('auth:api')->group(function () {
    // Manage Energy Types such as Gas and Electricity
    Route::resource('energy-types', 'EnergyTypeAPIController');

    // Manage clients endpoints
    Route::resource('clients', 'ClientAPIController');

    // Manage clients' readings endpoints
    Route::resource('readings', 'ReadingAPIController');
    
    // Get all readings of a given client
    Route::get('clients/{id}/readings', 'ReadingAPIController@getClientReadings');

    // Get logged in user information
    Route::get('/user-profile', function (Request $request) {
        return $request->user();
    });
});

