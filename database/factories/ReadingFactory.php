<?php

use Faker\Generator as Faker;

/**
 * Reading factory.
 * Used in seeding the readings table.
 */

$factory->define(App\Reading::class, function (Faker $faker) {
    $client = \App\Client::inRandomOrder()->first();
    $energyType = \App\EnergyType::inRandomOrder()->first();

    return [
        'client_id'      => $client->id,
        'energy_type_id' => $energyType->id,
        'reading'        => $faker->randomFloat(2, 1234),
        'date'           => $faker->dateTime('now'),
    ];
});
