<?php

use Faker\Generator as Faker;

/**
 * EnergyType factory.
 * Used in seeding the energy_types table.
 */

$factory->define(App\EnergyType::class, function (Faker $faker) {
    return [
        'name'              => 'Energy Name ' . $faker->numberBetween(2, 100),
        'is_enabled'        => $faker->boolean(50)
    ];
});
