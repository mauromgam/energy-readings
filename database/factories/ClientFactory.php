<?php

use Faker\Generator as Faker;

/**
 * Client factory.
 * Used in seeding the clients table.
 */

$factory->define(App\Client::class, function (Faker $faker) {
    return [
        'account_number' => $faker->randomNumber(7),
        'first_name'     => 'Client',
        'last_name'      => 'LastName ' . $faker->numberBetween(2, 1000),
        'phone_number'   => $faker->phoneNumber,
        'email'          => $faker->email,
        'default_reading_date'   => $faker->dateTimeInInterval(
            '-' . $faker->numberBetween(2, 5) . ' years',
            '+' . $faker->numberBetween(2, 90) . ' days'
        ),
        'is_enabled'  => $faker->boolean(50),
    ];
});
