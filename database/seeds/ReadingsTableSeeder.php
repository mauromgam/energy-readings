<?php

use App\Reading;
use Illuminate\Database\Seeder;

class ReadingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     * @throws Exception
     */
    public function run()
    {
        $clients = \App\Client::all();
        $energyTypes = \App\EnergyType::all();
        foreach ($clients as $client) {
            foreach ($energyTypes as $energyType) {
                $double = mt_rand() / mt_getrandmax();
                $reading = floatval(number_format(random_int(100, 99999) + $double, 4));
                $date = \Illuminate\Support\Carbon::now()
                    ->subDays(random_int(2, 1825));
                for ($i = 0; $i < 50; $i++) {
                    if ($date > \Illuminate\Support\Carbon::now()) {
                        break;
                    }
                    factory(Reading::class)->create([
                        'client_id'      => $client->id,
                        'energy_type_id' => $energyType->id,
                        'reading'        => $reading,
                        'date'           => $date,
                    ]);

                    $double = mt_rand() / mt_getrandmax();
                    $reading += floatval(number_format(random_int(50, 150) + $double, 4));
                    $date = $date->addMonth(1);
                }
            }
        }
    }
}
