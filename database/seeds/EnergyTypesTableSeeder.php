<?php

use App\EnergyType;
use Illuminate\Database\Seeder;

class EnergyTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $names = [
            'Gas',
            'Electricity'
        ];

        foreach ($names as $name) {
            factory(EnergyType::class)->create([
                'name'   => $name,
                'is_enabled' => 1,
            ]);
        }
    }
}
