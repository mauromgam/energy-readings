<?php

use Illuminate\Database\Seeder;

class OAuthClientsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $appName = env('APP_NAME');

        DB::table('oauth_clients')->insert([
            'id'                     => 1,
            'user_id'                => null,
            'name'                   => $appName,
            'secret'                 => 'Tl797ZmD2qFVlZQv0IVB9c3AIlKjEfKXQrbFEYpc',
            'redirect'               => 'http://localhost',
            'personal_access_client' => false,
            'password_client'        => true,
            'revoked'                => false,
            'created_at'             => date('Y-m-d H:i:s'),
            'updated_at'             => date('Y-m-d H:i:s'),
        ]);
    }
}
